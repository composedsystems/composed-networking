﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Messages to ServiceLedger" Type="Folder">
		<Item Name="Offer Service Msg.lvclass" Type="LVClass" URL="../ServiceLedger Messages/Offer Service Msg/Offer Service Msg.lvclass"/>
		<Item Name="Register Peer Msg.lvclass" Type="LVClass" URL="../ServiceLedger Messages/Register Peer Msg/Register Peer Msg.lvclass"/>
		<Item Name="Register Service Msg.lvclass" Type="LVClass" URL="../ServiceLedger Messages/Register Service Msg/Register Service Msg.lvclass"/>
		<Item Name="Request Service Msg.lvclass" Type="LVClass" URL="../ServiceLedger Messages/Request Service Msg/Request Service Msg.lvclass"/>
		<Item Name="Revoke Service Msg.lvclass" Type="LVClass" URL="../ServiceLedger Messages/Revoke Service Msg/Revoke Service Msg.lvclass"/>
		<Item Name="Unregister Service Msg.lvclass" Type="LVClass" URL="../ServiceLedger Messages/Unregister Service Msg/Unregister Service Msg.lvclass"/>
	</Item>
	<Item Name="Messages to ServiceListener" Type="Folder">
		<Item Name="Add Remote Service Msg.lvclass" Type="LVClass" URL="../IServiceListener Messages/Add Remote Service Msg/Add Remote Service Msg.lvclass"/>
		<Item Name="Receive Service Update Msg.lvclass" Type="LVClass" URL="../IServiceListener Messages/Receive Service Update Msg/Receive Service Update Msg.lvclass"/>
		<Item Name="Remove Remote Service Msg.lvclass" Type="LVClass" URL="../IServiceListener Messages/Remove Remote Service Msg/Remove Remote Service Msg.lvclass"/>
	</Item>
	<Item Name="Messages to ServiceProvider" Type="Folder">
		<Item Name="Add Service Msg.lvclass" Type="LVClass" URL="../IServiceProvider Messages/Add Service Msg/Add Service Msg.lvclass"/>
		<Item Name="Remove Service Msg.lvclass" Type="LVClass" URL="../IServiceProvider Messages/Remove Service Msg/Remove Service Msg.lvclass"/>
	</Item>
	<Item Name="IBeacon.lvclass" Type="LVClass" URL="../IBeacon/IBeacon.lvclass"/>
	<Item Name="IServiceListener.lvclass" Type="LVClass" URL="../IServiceListener/IServiceListener.lvclass"/>
	<Item Name="IServiceProvider.lvclass" Type="LVClass" URL="../IServiceProvider/IServiceProvider.lvclass"/>
	<Item Name="ISocketReader.lvclass" Type="LVClass" URL="../ISocketReader/ISocketReader.lvclass"/>
	<Item Name="ISocketWriter.lvclass" Type="LVClass" URL="../ISocketWriter/ISocketWriter.lvclass"/>
	<Item Name="ServiceLedger.lvclass" Type="LVClass" URL="../ServiceLedger/ServiceLedger.lvclass"/>
</Library>
